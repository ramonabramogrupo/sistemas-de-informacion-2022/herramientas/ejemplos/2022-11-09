﻿USE compranp;

-- CREO LA VISTA1
CREATE VIEW vista1 AS
  SELECT * FROM productos p;

-- EJECUTO LA VISTA1
SELECT * FROM vista1;

-- VISTA2
-- CREAR UNA VISTA LLAMADA PRODUCTOSVENDIDOS
-- DONDE ME MUESTRE EL NOMBRE DE LOS PRODUCTOS VENDIDOS

-- CREO LA CONSULTA DE SELECCION

SELECT 
    DISTINCT p.nombre 
  FROM 
    productos p JOIN compran c USING(codPro);

-- CREO UNA VISTA CON LA CONSULTA DE SELECCION
CREATE VIEW productosVendidos AS
  SELECT 
      DISTINCT p.nombre 
    FROM 
      productos p JOIN compran c USING(codPro);

-- COMPROBAR LA VISTA
SELECT * FROM productosVendidos v;

-- SI QUIERO MODIFICAR LA VISTA CREADA
DROP VIEW IF EXISTS productosVendidos;
CREATE VIEW productosVendidos AS
  SELECT 
      DISTINCT p.nombre 
    FROM 
      productos p JOIN compran c USING(codPro);

-- CREO UNA VISTA Y SI EXISTE LA REEMPLAZO
CREATE OR REPLACE VIEW productosVendidos AS
  SELECT 
      DISTINCT p.nombre 
    FROM 
      productos p JOIN compran c USING(codPro);

-- OPTIMIZAR LA CONSULTA ANTERIOR
-- NOMBRE DE LOS PRODUCTOS VENDIDOS

-- SACAR EL COD DE LOS PRODUCTOS VENDIDOS
-- C1
SELECT 
      DISTINCT c.codPro 
  FROM compran c;

-- NOMBRE DE LOS PRODUCTOS VENDIDOS 
-- JOIN ENTRE C1 Y PRODUCTOS
SELECT 
    p.nombre 
  FROM
      productos p 
    JOIN  
      (
        SELECT 
            DISTINCT c.codPro 
          FROM compran c
      ) C1 USING(codPro);

-- PUEDO CREAR LA VISTA CON ESTA CONSULTA??
-- SI
CREATE OR REPLACE VIEW productosVendidos AS 
  SELECT 
      p.nombre 
    FROM
        productos p 
      JOIN  
        (
          SELECT 
              DISTINCT c.codPro 
            FROM compran c
        ) C1 USING(codPro);

-- COMPROBAR LA VISTA
  SELECT * FROM productosVendidos v;


-- AÑADIR UNA RESTRICCION A LA TABLA PRODUCTOS
-- PARA QUE EL PRECIO TENGA QUE SER MAYOR QUE 1000

ALTER TABLE productos ADD CHECK (precio>0);

INSERT INTO productos (codPro, nombre, precio, numeroClientes)
  VALUES (250, 'pepinos', -1, 0);

SELECT * FROM productos p;

-- VOY A CREAR UNA VISTA QUE ME PERMITA 
-- EXIGIR QUE EL PRECIO SEA MAYOR QUE 0

CREATE OR REPLACE VIEW productosRestriccion AS 
  SELECT 
      p.codPro,
      p.nombre,
      p.precio,
      p.numeroClientes 
    FROM 
      productos p
    WHERE
        p.precio>0
    WITH LOCAL CHECK OPTION;

-- vuelvo a intentar meter los pepinos
-- para que la restriccion funcione los datos deben
-- entrar por la vista no por la tabla
INSERT INTO productosRestriccion (codPro, nombre, precio, numeroClientes)
  VALUES (250, 'pepinos', -1, 0);


